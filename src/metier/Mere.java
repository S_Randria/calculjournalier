/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import cache.Cache;


/**
 *
 * @author Sanda
 */
public abstract class Mere extends javax.swing.JPanel {
    private Mere fenetreApres;
    
    public abstract void ajouter()throws Exception;
    public abstract void suivant()throws Exception;
    public abstract void terminer()throws Exception;
    public abstract void boutton();

    public Mere() {
        boutton();
    }
    
    public Mere getFenetreApres() {
        return fenetreApres;
    }

    public void setFenetreApres(Mere fenetreApres) {
        this.fenetreApres = fenetreApres;
    }

    /**
     * Pour vérifier le contenu du calcul final
     */
    public void lecture(){        
        Cache cache = Cache.getInstance();
        Calcul calcul = (Calcul) cache.get("calcul");
        System.out.println("------------------------------------------");
        System.out.println(calcul);
        System.out.println("------------------------------------------");
    }
}
