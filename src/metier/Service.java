package metier;


import java.sql.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Sanda
 */
public class Service {
    public static Date dateDuJour()throws java.lang.Exception{
        java.util.Date d= new java.util.Date();
        int jour = d.getDate();
        int mois = d.getMonth() ;
        int annee = d.getYear() ;
        return new Date(annee,mois,jour);
    }
    public static Date stringToDate(String dateString)throws java.lang.Exception{
        Date d = dateDuJour();
        if(dateString != null && !dateString.equals("")){
            try{
                d = Date.valueOf(dateString);
            }catch(java.lang.IllegalArgumentException e){
                throw new java.lang.Exception("Format de date invalide, réessayez avec le format suivant \"yyyy-mm-dd\"");
            }
        }
        return d;
    }
    public static Object[][] calculDetailToTab(CalculDetail calcD){
        Object[][] tab = new Object[1][];
        int taille = 0, nouvelleTaille = 2;
        Integer montant  = 0;
        if(calcD != null){
            List duo = calcD.getValue();
            taille = duo.size();
            nouvelleTaille+= taille ;
            tab = new Object[nouvelleTaille][1] ;
            montant = calcD.getMontant();
            Duo temp = null;
            for(int i=0; i<taille; i++){
                temp = (Duo) duo.get(i);
                tab[i] = new String[]{temp.getMotif(),Montant.integerToString(temp.getMontant())};
            }
        }
        tab[taille]= new String[]{"",""};
        tab[taille+1]= new String[]{"Somme",Montant.integerToString(montant)};
        return tab;
    }
    /**
     * Inverse string , copy last letter to first letter
     * @param inverse
     * @return 
     */
    public static String inverse(String inverse){
        String newValue = "";
        int taille = inverse.length() -1;
        for(int i=taille; i >= 0; i--){
            newValue += inverse.charAt(i);
        }
        return newValue;
    }
    /**
     * 
     * @param value
     * @param parameter
     * @return 
     */
    public static String[] split(String value,String parameter){
        String newParameter = parameter,
                newValue = value;
        if(parameter.equals(".")){
            newParameter = ",";
            newValue = value.replace(".", newParameter); // split ne gère pas le paramètre "." alors on splite par virgule
        }
        String[] splitVirgule = newValue.split(newParameter);
        return splitVirgule;
    }
    public static void controlChiffre(String montant) throws java.lang.Exception{
        int taille = montant.length();
        String erreur = "";
        for(int i=0; i<taille; i++){
            if(!Constante.chiffre.contains(montant.charAt(i)+"")){
                erreur += montant.charAt(i)+" " ;
            }
        }
        if(!erreur.equals(""))
            throw new Exception(erreur+" n'est pas reconnu en tant que chiffre");
    }
}

