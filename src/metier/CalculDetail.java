/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Sanda
 */
public class CalculDetail {
    private static CalculDetail calcul;
    private List<Duo> value;
    private Integer montant;
    private String titre ;

    private CalculDetail() {
        this.initialize();
    }

    public static CalculDetail get() {
       if(CalculDetail.calcul == null) {
           calcul = new CalculDetail();
       }
       return calcul;
    }
    
    private void initialize(){
        this.value = new ArrayList();
        this.setMontant(0);
    }
    
    public Integer getMontant() {
        return this.montant;
    }
    public void add(String motif,String montant) throws Exception{
        Duo duo = new Duo(motif, montant);
        this.value.add(duo);
        this.addMontant(duo);
    }
        
    private void setMontant(Integer montant){
        this.montant = montant;
    }
    
    private void addMontant(Duo duo) throws Exception{
        Integer value = this.getMontant() + duo.getMontant();
        this.setMontant(value);
    }

    public List<Duo> getValue() {
        return value;
    }

    public static void destroy() throws Exception{
        CalculDetail.calcul = null;
    }

    public static CalculDetail getCalcul() {
        return CalculDetail.calcul;
    }

    public static void setCalcul(CalculDetail calcul) {
        CalculDetail.calcul = calcul;
    }

    public String getTitre() {
        return titre;
    }

    void setTitre(String titre) {
        this.titre = titre;
    }
    
    @Override
    public String toString(){
        String versionString = "";
        int taille = value.size();
        Duo duo = null;
        for(int i=0; i<taille; i++){
            duo = (Duo) value.get(i);
            versionString += duo;
        }
        versionString += "Somme: "+Montant.integerToString(this.getMontant());
        return versionString;
    }
    
}
