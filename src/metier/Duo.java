/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Sanda
 */
public class Duo {
    private String motif;
    private Integer montant;

    public Duo(String motif, String montant) throws Exception {
        this.setMotif(motif);
        this.setMontant(montant);
    }

    private void setMotif(String motif)throws Exception {
        if(motif == null || motif.equals("") == true) 
            throw new Exception("Motif vide");
        this.motif = motif;
    }

    private void setMontant(String montant)throws Exception {
        if(montant == null || montant.equals("") == true) 
            throw new Exception("montant vide");
        Integer value = Montant.stringToInteger(montant);
        this.setMontant(value);
    }

    private void setMontant(Integer montant){
        this.montant = montant;
    }

    public String getMotif() {
        return motif;
    }

    public Integer getMontant() {
        return montant;
    }
    /**
     * Aligner les chiffres
     * @param tailleMax
     * @return 
     */
    public String getMontantString(int tailleMax) {
        String montantString = Montant.integerToString(this.getMontant());
        int tailleManquant = tailleMax - montantString.length();
        String nouveauMontant = "";
        for(int i=0; i<tailleManquant; i++){
            nouveauMontant += " ";
        }
        nouveauMontant += montantString;
        return nouveauMontant;
    }
    
    @Override
    public String toString(){
        return this.getMotif() + " | " + this.getMontant() +"\n";
    }
    
}
